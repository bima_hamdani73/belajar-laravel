<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf                                       <!--supaya unik setiap pengirimannya-->
        <label>First Name:</label> <br><br>
        <input type="text" name="first_name">
            <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="last_name">
            <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other 
           <br><br>
        <label>Nationality:</label> <br><br>
        <select name="N">
            <option>Indonesian</option> 
            <option>Malaysian</option> 
            <option>Japanese</option> 
            <option>Chinese</option> 
            <option>Russian</option> 
            <option>Italian</option> 
        </select><br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other
            <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="35" rows="11"></textarea>
        <br>
        <input type="submit", value="Sign Up">
    </form>
</body>
</html>