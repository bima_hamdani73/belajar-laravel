@extends('layout.master')
 
@section('secondtitle')
    Tambah Pemain Film
@endsection('title')

@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                      <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
                @error('umur')
                  <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Bio</label>
                <textarea class="form-control" name="bio" rows="13" placeholder="Tuliskan bio"></textarea>
                @error('bio')
                <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                  {{ $message }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection